<?php

namespace App\Http\Controllers;

use App\Ciudades;
use App\Festivos;
use App\Pqr;
use App\PqrRespuesta;
use App\Radicacion;
use App\TpDocumento;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;

class PQRController extends Controller
{
    public function index()
    {
        $tpc = TpDocumento::all();
        $radicacion = Radicacion::all()->first()->consecutivo;
        $radicacion_final = $this->zero_fill($radicacion . Carbon::now()->format('Y-m-d'), 15);
        $ciudades = Ciudades::all();

        return view('pqr.ingresar', ['documentos' => $tpc, 'radicacion' => $radicacion_final, 'ciudades' => $ciudades]);
    }

    public function uploadDocs(Request $request)
    {
        $file = $request->file;
        $fecha = Carbon::now();

        if ($file->getClientOriginalExtension() == "pdf") {
            $destinationPath = public_path() . '/docs/documentos';
            $relative = '/docs/documentos/';
            $fileData = preg_replace('/\s+/', '', $request->cedula . trim($fecha) . $file->getclientoriginalname());

            $radicacion = Radicacion::where('id', 1)->first();
            $radicacion->consecutivo = $radicacion->consecutivo + 1;
            $radicacion->update();

            $pqr = new Pqr();

            $pqr->cedula = $request->cedula;
            $pqr->nombre = $request->nombre;
            $pqr->apellido = $request->apellido;
            $pqr->calidad = $request->calidad;
            $pqr->direccion = $request->direccion;
            $pqr->corregimiento = $request->corregimiento;
            $pqr->ciudad = $request->ciudad;
            $pqr->telefono = $request->telefono;
            $pqr->email = $request->email;
            $pqr->fecha_recepcion = $fecha;
            $pqr->no_radicacion = $request->no_radicacion;
            $pqr->respondido = "NO";
            $pqr->archivo_ruta = $relative . $fileData;
            $pqr->archivo_nombre = $fileData;

            $tpd = TpDocumento::where('id', $request->tpdoc)->first()->id;

            $pqr->tp_doc = $tpd;

            try {
                $pqr->save();
                $file->move($destinationPath, $fileData);
                Session::put('success', 'El Archivo ha subido correctamente!');
                return redirect()->route('pqr');
            } catch (\Exception $e) {
                Session::put('error', 'Se Produjo un error al subir archivo.');
                return redirect()->route('pqr');
            }
        } else {
            Session::put('error', 'Archivo No Valido, Debe tener la extension .PDF');
            return redirect()->route('pqr');
        }
    }


    public function respuestas()
    {
        $tpc = TpDocumento::all();
        $flag = false;

        $documentos = Pqr::where('respondido', 'NO')
            ->where(function ($query) {
                $query->where('tp_doc', '=', 1)
                    ->orWhere('tp_doc', '=', 2);
            })->get();

        if (Input::get('tp_doc')) {
            $input = Input::get('tp_doc');
            if (Input::get('tp_doc') == 0) {
                $documentos = Pqr::where('respondido', 'NO')
                    ->get();
            } else {
                $documentos = Pqr::where('respondido', 'NO')
                    ->where('tp_doc', $input)
                    ->get();
            }
        }

        $pendientes = Pqr::where('respondido', 'NO')
            ->where(function ($query) {
                $query->where('tp_doc', '=', 1)
                    ->orWhere('tp_doc', '=', 2);
            })
            ->get()->count();

        $array = [];
        $collection = null;

        foreach ($documentos as $doc) {
            $fecha = new Carbon($doc->fecha_recepcion);
            $flr = explode(' ', $this->getFiveDays($fecha, $fecha, 10) . '')[0];
            $fecha_frl = new Carbon($flr);
            $file = $doc->archivo_nombre;
            array_push($array, (object)array(
                'id' => $doc->id,
                'nombre' => $doc->nombre . ' ' . $doc->apellido,
                'cedula' => $doc->cedula,
                'calidad' => $doc->calidad,
                'direccion' => $doc->direccion,
                'email' => $doc->email,
                'fecha_recepcion' => $doc->fecha_recepcion,
                'no_radicacion' => $doc->no_radicacion,
                'respondido' => $doc->respondido,
                'archivo_ruta' => $file,
                'tp_doc' => $this->getTpDoc($doc->tp_doc),
                'flr' => $flr,
                'restantes' => $fecha_frl->diffInDays(Carbon::now()),
            ));
        }

        $collection = new Collection($array);

        if (Input::get('tp_doc')) {
            $flag = true;
            return view('pqr.respuesta', [
                'documentos' => $collection,
                'pendientes' => $pendientes,
                'docs' => $tpc,
                'flag' => $flag,
                'tp_doc' => Input::get('tp_doc')
            ]);
        }

        return view('pqr.respuesta', [
            'documentos' => $collection,
            'pendientes' => $pendientes,
            'docs' => $tpc,
            'flag' => $flag,
        ]);
    }

    public function uploadRespuesta(Request $request)
    {
        $file = $request->file;
        $fecha = Carbon::now();

        if ($file->getClientOriginalExtension() == "pdf") {
            $destinationPath = public_path() . '/docs/respuestas';
            $relative = '/docs/respuestas/';
            $fileData = preg_replace('/\s+/', '', $request->cedula . trim($fecha) . $file->getclientoriginalname());
            try {
                $pqr = Pqr::where('id', $request->id)->first();
                $pqr->respondido = "SI";
                $pqr->update();

                $respuestas = new PqrRespuesta();
                $respuestas->respuesta_ruta = $relative . $fileData;
                $respuestas->respuesta_nombre = $fileData;
                $respuestas->fecha = $fecha;
                $respuestas->documento = $request->id;

                $respuestas->save();
                $pqr->respuesta = $respuestas->id;
                $pqr->update();
                $file->move($destinationPath, $fileData);
                Session::put('success', 'El Archivo ha subido correctamente!');
                return redirect()->route('pqr.respuestas');
            } catch (\Exception $e) {
                Session::put('error', 'Se Produjo un error al subir archivo.');
                return redirect()->route('pqr.respuestas');
            }
        } else {
            Session::put('error', 'Archivo No Valido, Debe tener la extension .PDF');
            return redirect()->route('pqr.respuestas');
        }
    }

    public function consultas()
    {
        $perPage = 5;
        $page = Input::get('page');
        $pageName = 'page';
        $page = Paginator::resolveCurrentPage($pageName);
        $offSet = ($page * $perPage) - $perPage;

        $documentos = Pqr::where('respondido', 'SI')->offset($offSet)->limit($perPage)->orderByDesc('id')->get();

        $total_registros = Pqr::where('respondido', 'SI')->get()->count();

        $dp = Pqr::where('tp_doc', '1')->get()->count();
        $at = Pqr::where('tp_doc', '2')->get()->count();
        $it = Pqr::where('tp_doc', '3')->get()->count();
        $ii = Pqr::where('tp_doc', '4')->get()->count();
        $comp = Pqr::where('tp_doc', '5')->get()->count();
        $otro = Pqr::where('tp_doc', '6')->get()->count();

        $flag = false;

        if (Input::get('search')) {
            $input = Input::get('search');
            $documentos = Pqr::where('respondido', 'SI')
                ->where('cedula', $input)
                ->orWhere('apellido', 'LIKE', '%' . $input . '%')
                ->get();

            $flag = true;
        }

        $array = [];
        $collection = null;

        foreach ($documentos as $doc) {
            $respuesta = PqrRespuesta::where('id', $doc->respuesta)->first();
            array_push($array, (object)array(
                'id' => $doc->id,
                'nombre' => $doc->nombre . ' ' . $doc->apellido,
                'cedula' => $doc->cedula,
                'calidad' => $doc->calidad,
                'fecha_recepcion' => $doc->fecha_recepcion,
                'fecha_respuesta' => $respuesta->fecha,
                'no_radicacion' => $doc->no_radicacion,
                'tp_doc' => $this->getTpDoc($doc->tp_doc),
                'ruta_documento' => $doc->archivo_ruta,
                'ruta_respuesta' => $respuesta->respuesta_ruta
            ));
        }

        $collection = new Collection($array);

        $posts = new LengthAwarePaginator($collection, $total_registros, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        return view('pqr.consulta', [
            'documentos' => $posts,
            'flag' => $flag,
            'dp' => $dp,
            'at' => $at,
            'it' => $it,
            'ii' => $ii,
            'comp' => $comp,
            'otro' => $otro
        ])->withPosts($posts);
    }

    public static function getTpDoc($doc)
    {
        $documento = TpDocumento::where('id', $doc)->first()->nombre;
        return $documento;
    }

    function zero_fill($valor, $long = 0)
    {
        return str_replace('-', '', str_pad($valor, $long, '0', STR_PAD_LEFT));
    }

    public function getFiveDays($fechaCarbon, $fechaCarbon2, $numDias)
    {
        $festivos = Festivos::all();

        $dias = $this->getDaysHabiles($fechaCarbon, $festivos, $numDias);

        $dateFinal = $this->calcularFechaHabilLaboral($festivos, $fechaCarbon2->addDays($numDias + $dias));;

        $fechaFinal = explode(' ', $dateFinal . "")[0];
        return $fechaFinal;
    }

    public function getDaysHabiles($fechaCarbon, $festivos, $numDias)
    {
        $contF = null;
        $contS = null;
        $contD = null;

        for ($i = 0; $i < $numDias; $i++) {
            $date = $fechaCarbon->addDays(1);

            foreach ($festivos as $festivo) {
                $diaF = $festivo->dia;
                $diaC = $date->day;
                $mesF = $festivo->mes;
                $mesC = $date->month;
                $anoF = $festivo->anio;
                $anoC = $date->year;

                if ($diaF == $diaC && $mesF == $mesC && $anoF == $anoC) {
                    $contF++;
                    break;
                } else if ($fechaCarbon->shortEnglishDayOfWeek == 'Sat') {
                    $contS++;
                    break;
                } else if ($fechaCarbon->shortEnglishDayOfWeek == 'Sun') {
                    $contD++;
                    break;
                }
            }

        }

        $countF = $contF + $contS + $contD;

        return $countF;
    }

    public function calcularFechaHabilLaboral($festivos, $fechaCarbon)
    {
        foreach ($festivos as $festivo) {
            if ($festivo->dia == $fechaCarbon->day && $festivo->mes == $fechaCarbon->month && $festivo->anio == $fechaCarbon->year) {
                $fechaCarbon = $fechaCarbon->addDays(1);
                $fechaCarbon = $this->calcularFechaHabilLaboral($festivos, $fechaCarbon);
                break;
            } else if ($fechaCarbon->shortEnglishDayOfWeek == 'Sat') {
                $fechaCarbon = $fechaCarbon->addDays(2);
                $fechaCarbon = $this->calcularFechaHabilLaboral($festivos, $fechaCarbon);
                break;
            } else if ($fechaCarbon->shortEnglishDayOfWeek == 'Sun') {
                $fechaCarbon = $fechaCarbon->addDays(1);
                $fechaCarbon = $this->calcularFechaHabilLaboral($festivos, $fechaCarbon);
                break;
            }
        }
        return $fechaCarbon;
    }
}
