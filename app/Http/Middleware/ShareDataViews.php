<?php

namespace App\Http\Middleware;

use App\Menu;
use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ShareDataViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Config::set('database.default', 'mysql');
        DB::purge('mysql');

        $menus = Menu::all();
        $padres_hijos = [];

        foreach ($menus as $menu) {
            if ($menu->padre == null) {
                array_push($padres_hijos, (object)array(
                    'id' => $menu->id,
                    'nombre' => $menu->nombre,
                    'descripcion' => $menu->descripcion,
                    'ruta' => $menu->ruta,
                    'ruta_name' => $menu->ruta_name,
                    'icon' => $menu->icon,
                    'padre' => $menu->padre,
                    'hijos' => $menu->getHijos($menu->id)
                ));
            }
        }

        \View::share('menus', $padres_hijos);

        Config::set('database.default', 'mysql2');
        DB::purge('mysql2');

        return $next($request);

    }
}
