<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TpDocumento extends Model
{
    protected $table = 'pqr_tipo_documentos';
}
