# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Base de datos: xiltrionv2
# Tiempo de Generación: 2019-01-14 20:52:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `descripcion` varchar(350) DEFAULT NULL,
  `ruta` varchar(350) DEFAULT NULL,
  `ruta_name` varchar(350) DEFAULT NULL,
  `icon` varchar(350) DEFAULT NULL,
  `padre` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;

INSERT INTO `menu` (`id`, `nombre`, `descripcion`, `ruta`, `ruta_name`, `icon`, `padre`, `active`)
VALUES
	(1,'PQR','PQR',NULL,NULL,'fa fa-envelope',NULL,1),
	(2,'Ingresar Documentos','Ingresar Documentos','/PQR','pqr','fa fa-file-upload',1,1),
	(3,'Peticiones Pendientes','Respuestas','/PQR/respuestas','pqr.respuestas','fa fa-file-signature',1,1),
	(4,'Consultar Peticiones','Consultas','/PQR/consultas','pqr.consultas','fa fa-search',1,1);

/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla perfil_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perfil_menu`;

CREATE TABLE `perfil_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_perfil` int(10) unsigned NOT NULL,
  `id_menu` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_perfil` (`id_perfil`),
  KEY `id_menu` (`id_menu`),
  CONSTRAINT `perfil_menu_ibfk_1` FOREIGN KEY (`id_perfil`) REFERENCES `perfiles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `perfil_menu_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla perfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perfiles`;

CREATE TABLE `perfiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `perfiles` WRITE;
/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;

INSERT INTO `perfiles` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`)
VALUES
	(1,'Caja','Acceso a caja','2017-09-01 06:35:24','2017-09-01 06:35:24'),
	(2,'Asistente Administrativo','Impuestos y Reportes','2017-09-01 06:36:03','2017-09-01 06:36:03'),
	(3,'Archivo','Acceso a Archivo',NULL,NULL),
	(4,'COACTIVO','COACTIVO','2017-09-05 12:16:08','2017-09-05 12:17:07'),
	(5,'SOCIOS','SOCIOS','2017-10-20 15:59:50','2017-10-20 15:59:50'),
	(6,'Tramites','Tramites','2018-08-27 09:09:08','2018-08-27 09:29:48'),
	(7,'JURIDICA','ATENCION DE SOLICITUDES Y TUTELAS','2018-09-17 11:12:34','2018-09-17 11:12:34'),
	(8,'Contabilidad','Contabilidad','2018-10-11 11:40:16','2018-10-11 11:40:16');

/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_organismo` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secreto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `id_organismo`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `active`, `role`, `secreto`)
VALUES
	(1,1,'Administrador Sistema','admin@ettcurumani.com','$2y$10$SUy9cxaUtjxAlq1wdk10PO8JlOfJ4dMBBZ7Sq91BEZ9XHcvo5/8Gu','seDX27Z9665owg8B7b4Ipb8WvzKodMnPYOwiOXTwzNeppXS6nhNESxFeuFzE','2017-08-16 18:54:23','2017-09-19 13:35:10',1,'super','Q33U6CASLGVDSQRL'),
	(4,1,'Fredy Sosa2','fsosa@ettcurumani.com','$2y$10$SUy9cxaUtjxAlq1wdk10PO8JlOfJ4dMBBZ7Sq91BEZ9XHcvo5/8Gu','E5wcHTf71rdmZb5rKrSce6PvqRoIXSkoyyjULzwIFLJMc1INbFrOiC3bSUlH','2017-08-17 12:44:34','2018-01-06 21:47:28',1,'admin','CCCV5DBVMUW23EQT'),
	(5,1,'Soporte Xiltrion','soporte.xiltrion@ettcurumani.com','$2y$10$FiwC8IIFyIch9m4PmwlItu3CTo2h6PvoqP3s23LkhtymQPEQC4.bG',NULL,'2018-08-24 09:23:06','2018-08-24 09:25:19',1,'user','2TAQAA2CCUGFCP4J'),
	(6,1,'Soporte Xiltrion','soporte.xiltrion@datafis.com','$2y$10$i2CJ7TfmuqTimGGbJRWw8elzfbEtMIxcb9O0xT/IFCoFnUYBc4i2m','5V4eHuLeUJRhDF2cZP4wxTKIPwjFRM4BkXvsrQKHGz1SZGVhBZPsaPFAU5Hh','2018-08-24 09:26:29','2018-08-27 09:27:02',0,'user','Y6NP33FGNAXJT7F5'),
	(7,1,'caja','caja@ettcurumani.com','$2y$10$kE2X0SNkjDs7pTAkMuoOauVQGB/QPkKlZU5NuaREyvzvG/BsGHFuS','UBjY0yCkVOBwi03zv0TEhGoxPPhzAx8nkrdT75lDJpSmfRkutvJC1XLa9qR6','2018-08-27 08:33:59','2018-09-19 09:10:58',1,'user','3OWFGGT63AOC5VUG'),
	(8,1,'RUNT','runt@ettcurumani.com','$2y$10$ZxoFhFnIHkJ9xBdm5zjVquTvq3WrWhzIcI1POOCppUupuxyVX/l/W','fpejt7IXgMUTskYtiY1erwxBCnygpL7mSmUWx4z9aueqDjFYju4cxM08ditY','2018-08-27 09:27:54','2018-08-27 10:53:59',1,'user','GGKZMH46NWR3XHPB'),
	(9,1,'ARCHIVO','archivo@ettcurumani.com','$2y$10$A4hYRLdS8oqs.YIFvH3FF.SKVbzRAxiN3evO8ASAIFtimrGfiCw2S','I1mTsq4SBxYz7pjSFYER5wxqck0SyljDYYVoEjZnSDFZEMi1YYxgTHjemr5q','2018-08-27 17:10:13','2018-09-17 16:35:09',1,'user','YJGGQBNZAHZFJS2S'),
	(10,1,'Maria Alejandra','mcastro@ettcurumani.com','$2y$10$zqqeZYiySTf5Pigt5k7IF.89BiX.So5fxR2h.u8Dkj1YRMoxAwdE6','VjuJQGLqz5tOu8W60phvgPHcrKVnFgYnZoKvrdNe7YK7lU6jo0qOPtzVXSZM','2018-09-13 08:55:19','2018-09-26 13:33:40',1,'user','WQT3IKXOB4LPMQTJ'),
	(11,1,'JURIDICA','juridica@ettcurumani.com','$2y$10$PykosZOvZTMZ0Nhx3JHk/OD2MgKH0TgMxuO.RoyGyhrFegtrjATAO','6vLuxR1l5fF8XTHKQbx1gCo2rSvHIp3Jg2pb72Z3tdu6jI3kZCWjxAtL0R8a','2018-09-17 11:09:09','2018-09-18 21:45:57',1,'user','IZV55UZMFAJK5QPE'),
	(12,1,'Validacion','validacion@ettcurumani.com','$2y$10$/MhEfVg86rRSLF1pMl0jCOxd7s5yNpsm0YtkNjCjMZTCZxSGVDu2y','dAOop13WZYvUbNJqvJzlc6cyVePZXbwPqBX79Gs0uxiOZUVPWyqrNmxA9yOb','2018-09-17 16:42:51','2018-09-17 16:42:57',1,'user','QDF3TW3OP4IB65W2'),
	(13,1,'Invertectra','invertectra@ettcurumani.com','$2y$10$lZccO80AT530A..kmTtHI.gZe3w6O9lDbdIgIBXi5oMPJV82Kj48O','oeTX20Ywnk9ZPnk8EXr5KRjRGGnoNBvF7l7qIjc5XfFqovDXbD8CXRxHfs3T','2018-09-20 09:42:46','2018-09-20 09:45:09',1,'user','BYQHMNRFID4N6Y2S'),
	(14,1,'Contabilidad','contabilidad@ettcurumani.com','$2y$10$uHc1oJCBeRaAZOcO19osKeMMTTr0.R7farxuGeXZCh.7r8AxnXTbO','CUVMuxFZEJJgIy7lqTRqu9G42ZarBgjIuVaVNUddShr7mbuosqGgXChJJY76','2018-10-11 11:37:36','2018-10-11 11:41:41',1,'user','MMXVOAFTRIMFHEWB'),
	(15,1,'RUNT2','runt2@ettcurumani.com','$2y$10$swBf5/mkipY8Kh.xTvguWuLMNnb4mGnHfmQok7GjL7OI.DfFBaSme','8irRvqUip3ZN0xIgPilWeUGRCd7SOnRzVdaAyM45wEaI8jptldyRD2nBsNSf','2018-10-31 09:40:59','2018-10-31 09:42:42',1,'user','XTLNG2UFVRFE3KK5'),
	(16,1,'Julio Pe?a','jsarmiento@ettcurumani.com','$2y$10$sl81ZcBj3kgCn06UlLceDOSb.Nsu3.b9DYn8tGn89mj4Ld23ETiTC','DcTuBChK4TwKQOIlKPBUY7ipoJ2UuQZR2j1cYEDEQMWXKCERldZKizSd5EQq','2018-12-14 15:48:50','2018-12-14 15:48:57',1,'user','4XPXVYCR5U6WFRJJ'),
	(17,1,'Asistente','asistente@ettcurumani.com','$2y$10$/DjeFWAEpcLi1P.LSRAT4uH4/sG3OrGZ2k9NmuHw4CJwcmCWykKUu','DQSGCjHxLBeTmHYjWab7eHjedk8MGGdXhGniK21E0VcBWMWySLbPSu3TYzRZ','2018-12-14 16:04:48','2018-12-14 16:04:54',1,'user','LXIKBUOA3JDRST47');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
