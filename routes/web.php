<?php

use \Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware' => 'shareViews'], function () {
    Route::group(['middleware' => ['auth']], function () {

        Route::get('/', function () {
            if (isset(\Illuminate\Support\Facades\Auth::user()->id)) {
                return view('home');
            } else {
                return view('auth.login');
            }
        });

        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/PQR/index', 'PQRController@index')->name('pqr');
        Route::post('/PQR/doc/upload', 'PQRController@uploadDocs')->name('pqr.doc.upload');
        Route::get('/PQR/respuestas', 'PQRController@respuestas')->name('pqr.respuestas');
        Route::post('/PQR/respuestas/upload', 'PQRController@uploadRespuesta')->name('pqr.respuestas.upload');
        Route::get('/PQR/getDays/{p1}/{p2}', 'PQRController@getFiveDays')->name('pqr.getDays');
        Route::get('/PQR/consultas', 'PQRController@consultas')->name('pqr.consultas');
        Route::get('/reportes', 'PQRController@consultas')->name('reportes');
        Route::get('/reportes/recaudo', 'PQRController@consultas')->name('reporte.recaudo');
        Route::get('/reportes/cartera', 'PQRController@consultas')->name('reporte.cartera');
    });
});

