@extends('layouts.app')
@section('breadcrumbs')
    @include('__partials.breadcrumbs', [
        'data' => [
            ['route' => 'home', 'name' => 'Home'],
            ['route' => '', 'name' => 'PQR'],
            ['route' => '', 'name' => 'Consultar Peticiones']
        ],
        'show_add_button' => false,
        'add_button_route' => route('pqr.respuestas')
    ])
@endsection
@section('content')
    <style>
        table tbody tr td, table thead tr th {
            font-size: 11px !important;
            padding-right: 0px !important;
        }

        form {
            padding: 0 !important;
        }

        .table th, .table td {
            padding: 10 !important;
        }

    </style>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <center><h5>CONSULTA</h5></center>
                    <form action="{{route('pqr.consultas')}}" method="get">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Cedula / Apellido</label>
                                <input name="search" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <br>
                                <button class="btn btn-outline-success" style="margin-top: 8px !important;">Buscar
                                </button>
                            </div>
                            @if($flag)
                                <div class="col-md-2">
                                    <br>
                                    <button class="btn btn-outline-info" style="margin-top: 8px !important;">Volver
                                    </button>
                                </div>
                            @endif
                        </div>
                    </form>
                    <br>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-1 alert alert-light" role="alert" style="text-align: center">
                            <strong style="color: #0a0c14" title="DERECHO PETICION">DP</strong> <span class="badge badge-primary">{{$dp}}</span>
                        </div>
                        <div class="col-md-1 alert alert-light" role="alert" style="text-align: center">
                            <strong style="color: #0a0c14" title="ACCION DE TUTELA">AT</strong> <span class="badge badge-primary">{{$at}}</span>
                        </div>
                        <div class="col-md-1 alert alert-light" role="alert" style="text-align: center">
                            <strong style="color: #0a0c14" title="INCIDENTE DE TRANSITO">IT<br></strong> <span class="badge badge-primary">{{$it}}</span>
                        </div>
                        <div class="col-md-1 alert alert-light" role="alert" style="text-align: center">
                            <strong style="color: #0a0c14" title="INFORME IPAT">II<br></strong> <span class="badge badge-primary">{{$ii}}</span>
                        </div>
                        <div class="col-md-1 alert alert-light" role="alert" style="text-align: center">
                            <strong style="color: #0a0c14" title="COMPARENDO">COM</strong> <span class="badge badge-primary">{{$comp}}</span>
                        </div>
                        <div class="col-md-1 alert alert-light" role="alert" style="text-align: center">
                            <strong style="color: #0a0c14" title="OTRO">OTR</strong> <span class="badge badge-primary">{{$otro}}</span>
                        </div>
                    </div>
                    <br>
                    <div class="row table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">F. Recepcion</th>
                                <th scope="col">F. Respuesta</th>
                                <th scope="col"># Radicacion</th>
                                <th scope="col">Cedula</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Tipo Dcto</th>
                                <th style="text-align: center !important;" scope="col">Documento</th>
                                <th style="text-align: right !important;" scope="col">Respuesta</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($documentos as $doc)
                                <tr>
                                    <td><b>{{$doc->id}}</b></td>
                                    <td>{{$doc->fecha_recepcion}}</td>
                                    <td>{{$doc->fecha_respuesta}}</td>
                                    <td>{{$doc->no_radicacion}}</td>
                                    <td>{{$doc->cedula}}</td>
                                    <td>{{$doc->nombre}}</td>
                                    <td>{{$doc->tp_doc}}</td>
                                    <td>
                                        <div style="height: 100%" class="modal fade bd-example-modal-lg"
                                             id="modal{{$doc->id}}" tabindex="-1" role="dialog"
                                             aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <iframe class="embed-responsive-item"
                                                                src="{{asset($doc->ruta_documento)}}"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                                data-target="#modal{{$doc->id}}"><i class="fa fa-eye fa-2x"></i>
                                        </button>
                                    <td>
                                    <td>
                                        <div style="height: 100%" class="modal fade bd-example-modal-lg"
                                             id="modal2{{$doc->id}}" tabindex="-1" role="dialog"
                                             aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <iframe class="embed-responsive-item"
                                                                src="{{asset($doc->ruta_respuesta)}}"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                                data-target="#modal2{{$doc->id}}"><i class="fa fa-eye fa-2x"></i>
                                        </button>
                                    <td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(!$flag){{ $documentos->appends([])->links() }}@endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
