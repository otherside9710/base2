@extends('layouts.app')
@section('breadcrumbs')
    @include('__partials.breadcrumbs', [
        'data' => [
            ['route' => 'home', 'name' => 'Home'],
            ['route' => '', 'name' => 'PQR'],
            ['route' => '', 'name' => 'Ingresar Documentos']
        ],
        'show_add_button' => false,
        'add_button_route' => route('pqr.respuestas')
    ])
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <center><h4>INGRESAR DOCUMENTOS</h4></center>
                    <br><br>
                    <form id="form" method="post" action="{{route('pqr.doc.upload')}}"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <label>Cedula</label>
                                <input class="form-control" required name="cedula">
                            </div>
                            <div class="col-md-3">
                                <label>Nombres</label>
                                <input class="form-control" required name="nombre">
                            </div>
                            <div class="col-md-4">
                                <label>Apellidos</label>
                                <input class="form-control" required name="apellido">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <label>Quien Radica?</label>
                                <select class="form-control" required name="calidad">
                                    <option value="Nombre Propio">Nombre Propio</option>
                                    <option value="Apoderado">Apoderado</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Tipo Documento</label>
                                <select class="form-control" required name="tpdoc">
                                    @foreach($documentos as $doc)
                                        <option value="{{$doc->id}}">{{$doc->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Direccion</label>
                                <input class="form-control" required name="direccion">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <label>Corregimiento / Vereda</label>
                                <input class="form-control" name="corregimiento">
                            </div>
                            <div class="col-md-3">
                                <label>Ciudad / Municipio</label>
                                <input class="form-control" required name="ciudad">
                            </div>
                            <div class="col-md-4">
                                <label>Telefono</label>
                                <input class="form-control" required name="telefono">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <label>Email</label>
                                <input type="email" class="form-control" required name="email">
                            </div>
                            <div class="col-md-3">
                                <label># Radicacion</label>
                                <input class="form-control" readonly value="{{$radicacion}}" name="no_radicacion">
                            </div>
                            <div class="col-md-4">
                                <label>Documento</label>
                                <input type="file" class="form-control" required name="file">
                            </div>
                        </div>

                        <br>
                        <center>
                            <button class="btn btn-outline-success">
                                Guardar
                            </button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
