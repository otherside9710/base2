@extends('layouts.app')
@section('breadcrumbs')
    @include('__partials.breadcrumbs', [
        'data' => [
            ['route' => 'home', 'name' => 'Home'],
            ['route' => '', 'name' => 'PQR'],
            ['route' => '', 'name' => 'Peticiones Pendientes']
        ],
        'show_add_button' => false,
        'add_button_route' => route('pqr.respuestas')
    ])
@endsection
@section('content')
    <style>
        table tbody tr td, table thead tr th {
            font-size: 11px !important;
            padding-right: 0px !important;
        }

        form {
            padding: 0 !important;
        }

        .table th, .table td {
            padding: 10 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <center><h4>PETICIONES PENDIENTES</h4></center>
                    <form action="{{route('pqr.respuestas')}}" method="get">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Tipo Documento</label>
                                <select class="form-control" name="tp_doc">
                                    <option value="0">Todos</option>
                                    @foreach($docs as $doc)
                                        <option
                                            value="{{$doc->id}}" @if($flag)
                                        @if($tp_doc == $doc->id)
                                        selected
                                            @endif
                                            @endif>{{$doc->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <button style="margin-top: 10px !important;" class="btn btn-outline-success">Buscar
                                </button>
                            </div>
                            <div class="col-md-5"></div>
                            <div class="col-md-2 alert alert-light" role="alert" style="text-align: center">
                                <strong style="color: #0a0c14">Pendientes</strong> <span
                                    class="badge badge-primary">{{$pendientes}}</span>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </form>
                    <br><br>
                    <div class="row table-responsive">
                        @if($documentos != "[]")
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Cedula</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Tipo Dcto</th>
                                    <th title="FECHA LIMITE DE RESPUESTA" scope="col">F.L.R</th>
                                    <th scope="col">Dias Faltantes</th>
                                    <th scope="col">Documento</th>
                                    <th style="text-align: center !important;" scope="col">Respuesta</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Large modal -->

                                @foreach($documentos as $doc)
                                    <tr>
                                        <td><b>{{$doc->id}}</b></td>
                                        <td>{{$doc->fecha_recepcion}}</td>
                                        <td>{{$doc->cedula}}</td>
                                        <td>{{$doc->nombre}}</td>
                                        <td>{{$doc->tp_doc}}</td>
                                        <td>{{$doc->flr}}</td>
                                        <td>{{$doc->restantes}}</td>
                                        <td>
                                            <div style="height: 100%" class="modal fade bd-example-modal-lg"
                                                 id="modal{{$doc->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <iframe class="embed-responsive-item"
                                                                    src="{{asset('/docs/documentos/'. $doc->archivo_ruta)}}"
                                                                    allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                                    data-target="#modal{{$doc->id}}"><i class="fa fa-eye fa-2x"></i>
                                            </button>
                                        <td>
                                            <form id="form" method="post" action="{{route('pqr.respuestas.upload')}}"
                                                  enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{$doc->cedula}}" name="cedula">
                                                <input type="hidden" value="{{$doc->id}}" name="id">
                                                <input type="file" class="form-control" required name="file">
                                                <center>
                                                    <button class="btn btn-outline-success">Subir</button>
                                                </center>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="col-md-4"></div>
                            <center><b>No Hay Peticiones por responder</b></center>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
