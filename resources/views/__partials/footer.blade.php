<footer class="footer">
  <div class="container-fluid clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
      <a href="http://www.datafis.com/" target="_blank">DATAFIS S.A.S.</a></span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Xiltrion
      <i class="far fa-copyright"></i>
    </span>
  </div>
</footer>
