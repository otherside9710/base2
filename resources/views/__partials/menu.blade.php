<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="{{asset('assets/images/faces/user.png')}}" alt="profile image"></div>
                    <div class="text-wrapper">
                        <p class="profile-name">{{\Illuminate\Support\Facades\Auth::user()->name}}  </p>
                        <div>
                            <small class="designation text-muted">Admin</small>
                            <span class="status-indicator online"></span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://xiltrion.com:444" target="_blank">
                <i class="menu-icon fa fa-times"></i>
                <span class="menu-title"><b>Xiltrion</b></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('home')}}">
                <i class="menu-icon fa fa-tachometer-alt"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        @foreach($menus as $menu)
            @if(Auth::user()->autorizarPerfil([$menu->ruta]))
                <li class="nav-item">
                    <a class="nav-link" @if($menu->hijos != "[]") data-toggle="collapse" @endif
                    @if($menu->hijos != "[]") href="#dashboard-dropdown{{$menu->id}}"
                       @else
                       href="{{route($menu->ruta_name)}}"
                       @endif
                       aria-expanded="false"
                       aria-controls="dashboard-dropdown">
                        <i class="menu-icon {{$menu->icon}}"></i>
                        <span class="menu-title">{{$menu->nombre}}</span>
                        @if($menu->hijos != "[]")
                            <i class="menu-arrow"></i>
                        @endif
                    </a>
                    <div class="collapse" id="dashboard-dropdown{{$menu->id}}">
                        <ul class="nav flex-column sub-menu">
                            @foreach($menu->hijos as $hijo)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route($hijo->ruta_name)}}"
                                       style="text-align: left !important; margin-left: -30px !important; width: 100% !important;"
                                       @if(Request::is($menu->ruta)) class="active" @endif>
                                        <i class="menu-icon {{$hijo->icon}}"></i>
                                        {{$hijo->nombre}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @endif
        @endforeach
    </ul>
</nav>
